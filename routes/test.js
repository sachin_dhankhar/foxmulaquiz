const express=require('express');
const router=express.Router();
const studentTest=require('../models/signup');

var Id;

router.get("/test", function(req, res){
    
    res.render("test");
});

router.post("/test", function(req, res){
    studentTest.find({},function(err, result){
        var StringId = result[result.length-1].testId;
        var Num = (Number)(StringId.substring(4, StringId.length))+1;
        Id = "test"+Num.toString();
	module.exports.testid2=Id;
        var newStudent = {testId:Id, name:req.body.name, email:req.body.email, contact: req.body.contact, college:req.body.college};
        studentTest.create(newStudent, function(err, studentTest){
            if(err){
                console.log(err);
            }
            else{
                res.redirect('/show');
            }
        });
    });   
});

module.exports={router:router,
		testid:Id
};

