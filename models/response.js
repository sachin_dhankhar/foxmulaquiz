const mongoose=require('mongoose');
const schema=mongoose.Schema;

const ResponseSchema=new schema({
	testid:{
		type:String
	},
	qids:[String],
	responses:[String],
	category:{
		categoryA:Number,
		categoryB:Number,
		categoryC:Number,
		categoryD:Number
	}

	
});
module.exports=mongoose.model('responseTest',ResponseSchema);