const mongodb=require('mongodb');
const express=require('express'); 
const bodyparser=require('body-parser');
const mongoose=require('mongoose');
const path=require('path');



const questionTest=require('./models/model');
const responseTest=require('./models/response');
const studentTest=require('./models/signup');


const addquestionRoute=require('./routes/addquestion');
const showtestRoute=require('./routes/showtest');
const resultRoute=require('./routes/result');
const testRoute=require('./routes/test');





var pdfGeneratorService=require('./pdfGenerationModule/pdfGeneratorService');





var counter=0;

app=express();
app.set('views',path.join(__dirname,'views'));
app.set('view engine','ejs');
mongoose.connect('mongodb://127.0.0.1:27017/nodejstest');
app.use(bodyparser.urlencoded({extended: false}));



app.post(addquestionRoute);

app.use('/adminaddquestion',(req,res,next)=>{
	res.render('adminaddquestion',{count:++counter});
});


app.use(resultRoute);


app.use(showtestRoute);



app.use(testRoute.router);

app.use('/pdf',(req,res,next)=>{
    pdfGeneratorService();
    res.send('<h1>now send email');//----------------------
});


app.use('/',(req,res,next)=>{
	res.send('<h1>hello from foxmula');
});

app.listen(3000);
