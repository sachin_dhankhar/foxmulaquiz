var dataGathererService = require('./dataGathererModule/dataGathererService');
var htmlGenerator = require('./pdfTemplate/htmlGenerator');
var generatePDF = require('./pdfGenerator/pdfGenerator');






function pdfGeneratorService(){
    var templateFile = './pdfGenerationModule/pdfTemplate/answer.ejs';
    var outputFile = './report' + 'pdf-' + new Date().getTime() + '.pdf';
    return dataGathererService().then(function (data) {
        return htmlGenerator(templateFile, data);
      }).then(function (html) {
        return generatePDF(html, outputFile);
      }).then(function (success) {
        console.log('PDF generation successful at ' + outputFile);
        return success;
      }).catch(function (err) {
        console.log('ERROR in PDF generation : ' + err);
        return false;
      });
}



module.exports = pdfGeneratorService;