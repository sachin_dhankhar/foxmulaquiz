const express=require('express');
const router=express.Router();
const questionTest=require('../models/model');

router.use('/show',(req,res,next)=>{
	questionTest.find({}).exec((err,obj)=>{
		if(err){
			console.log("error in /show");
		}
		else{
			res.render('showtest',{questiondb:obj});
		}
	});
	
});
module.exports=router;
